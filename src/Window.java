import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by amen on 12/1/17.
 */
public class Window {
    public Window() {
        kobietaRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // usun z combobox'a czesc opcji
                // dodaj pozostale
                stanCombo.removeAllItems();
                stanCombo.addItem("mężatka");
                stanCombo.addItem("singielka");
            }
        });
        mężczyznaRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // usun z combobox'a czesc opcji
                // dodaj pozostale
                stanCombo.removeAllItems();
                stanCombo.addItem("żonaty");
                stanCombo.addItem("singiel");
            }
        });
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // czytanie wartości pól
                // stworzenie string'a (z wartości pól) do zapisu do pliku
                // otwórz plik
                // zapisz dane
                // zamknij plik
                // !!! wyczyść wszystkie pola na domyślne wartości
                try{
                    String imie = imieField.getText();
                    String nazwisko = nazwiskoField.getText();
                    String plec = kobietaRadioButton.isSelected() ? "Kobieta" : "Mężczyzna";
                    int but = (int) butField.getValue();
                    String stanCyw = String.valueOf(stanCombo.getSelectedItem());
                    if(!mężczyznaRadioButton.isSelected() && !kobietaRadioButton.isSelected()){
                        throw new IllegalArgumentException("zaznacz cos");
                    }

                    try (PrintWriter writer = new PrintWriter(new FileWriter("out.txt"))) {
                        writer.println(imie);
                        writer.println(nazwisko);
                        writer.println(plec);
                        writer.println(but);
                        writer.println(stanCyw);
                    } catch (IOException ioe) {
                        System.out.println(ioe.getMessage());
                    }
                }catch (IllegalArgumentException iae ){
                    statusLabel.setText(iae.getMessage());
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                    statusLabel.setText("");

//                    Thread watek = new Thread(new Runnable() {
//                        @Override
//                        public void run() {
//                            try {
//                                Thread.sleep(5000);
//                            } catch (InterruptedException e1) {
//                                e1.printStackTrace();
//                            }
//                            statusLabel.setText("");
//                        }
//                    });
//                    watek.start();
                }

            }
        });
    }

    public JPanel getPanel1() {
        return panel1;
    }

    private JPanel panel1;
    private JPanel centerPanel;
    private JButton button1;
    private JTextField imieField;
    private JTextField nazwiskoField;
    private JRadioButton mężczyznaRadioButton;
    private JRadioButton kobietaRadioButton;
    private JSpinner butField;
    private JComboBox stanCombo;
    private JLabel statusLabel;
    private JLabel etykieta;
    private int counter = 0;
}
