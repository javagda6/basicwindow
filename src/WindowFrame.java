import javax.swing.*;
import java.awt.*;

/**
 * Created by amen on 12/1/17.
 */
public class WindowFrame extends JFrame {
    private Window interfejs; // graficzny interfejs

    public WindowFrame() throws HeadlessException {
        this.interfejs = new Window();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        setContentPane(interfejs.getPanel1());
        setMinimumSize(new Dimension(800, 600));

        pack();
    }
}
